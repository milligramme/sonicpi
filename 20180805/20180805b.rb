use_bpm 120

live_loop :d do
  with_fx :echo, phase: 0.25, amp: 3, mix: 0.9, max_phase: 0.5 do
    sample :bd_klub
    sleep 1
  end
end


live_loop :sn do
  sync :d
  
  with_fx :bitcrusher, bits: 8, sample_rate: 9600 do
    sample :tabla_ghe8
    sample :tabla_ghe7, amp: 0.3
    sleep 2
  end
end


live_loop :na do
  sync :d
  with_fx :reverb do
    knits = (knit 0.9, 3, 0.8, 2, 0.75, 6, 0.5, 4, 1, 6).sample
    sample :tabla_na, beat_stretch: knits
    sleep 4
  end
end
