# noise and slinder

live_loop :tick do
  sleep 2
end

live_loop :n do
  sync :tick
  
  use_synth :zawa
  with_fx :reverb, mix: 0.75, pre_mix: 0.45, room: 1 do
    with_fx :flanger, decay: 0.25, delay: 0.75, feedback: 0.125, wave: 1 do
      with_fx :bitcrusher, bits: 3, cutoff: rrand(70,120), sample_rate: 96 do
        with_fx :echo, decay: 0.7, mix: 0.9, phase: 0.25, max_phase: 0.5 do
          play chord :c3, :M7
          sleep 0.125
          play chord :c3, :minor7
          sleep 0.125
          play chord :f2, '7', release: 0.9
          sleep 3.75
        end
      end
    end
  end
end

live_loop :m do
  sync :tick
  
  use_synth :piano
  notes = [:c5, :g4, :f4, :d5, :e5, :r, :a4, :cs5, :d4, :e4, :r, :cs5]
  times = Array.new(notes.size, 0.25)
  with_fx :reverb, room: 1 do
    with_fx :echo, decay: 0.2, phase: [0.25, 0.5, 0.75].choose, mix: 0.9, amp: 0.6 do
      play_pattern_timed notes, times, amp: 0.6
    end
  end
end

live_loop :t do
  sync :tick
  
  use_synth :fm
  with_fx :distortion, distort: 0.9, amp: (knit 0.5, 1, 0.2, 15).choose do
    with_fx :gverb, dry: 0.25, release: 0.3, room: 1 do
      play chord(:a3, :M7)
      sleep 2
      play chord(:g3, '7')
      sleep 1.5
      play chord(:gs3, '7'), amp: 0.6
      sleep 0.25
    end
  end
end


