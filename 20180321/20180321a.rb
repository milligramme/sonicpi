use_bpm 120


MASTER_VOL = 1

def click_vol
  MASTER_VOL * 1
  0
end

def dist_guitar_vol
  MASTER_VOL * 0.8
end

def noise_vol
  MASTER_VOL * 0.01
end

def mod_chord_vol
  MASTER_VOL * 5
  0
end

def industrial_vol
  MASTER_VOL * 0.9
  0
end

def drum_bd_vol
  MASTER_VOL * 1.5
  0
end

def drum_sd_vol
  MASTER_VOL * 1
  0
end

def drum_hat_vol
  MASTER_VOL * 0.3
  0
end

live_loop :base do
  sleep 0.5
end






live_loop :click do
  with_fx :flanger, feedback: 0.25, mix: 0.7 do
    with_synth :pretty_bell do
      f = [:c2, :b2, :d3, :f2, :a2, :r, :cs2, :r]
      play f.ring.tick, attack: 0,
        decay_level: rrand(1, 2), decay: rrand(0.02, 0.25),
        sustain: rrand(0.02,0.05),
        release: 0, pan: range(-1, -0.5, 0.125).tick,
        amp: click_vol
      sleep 0.25
    end
  end
end

live_loop :dist_guitar do
  sync :base
  sleep 2
  with_synth :fm do
    with_fx :bitcrusher, bits: 4 do
      with_fx :panslicer, smooth: 1, smooth_down: 1 do
        with_fx :flanger, feedback: rrand(0.25, 0.7), mix: 0.9 do
          play [:f5, :fs5].sample,
            amp: :dist_guitar_vol,
            attack: range(0, 0.5, 0.125).mirror.tick,
            decay: 0.25, sustain: 0.1, release: 4
        end
      end
    end
  end
  sleep 14
end



live_loop :dist_noise do
  with_fx :reverb, mix: 0.25, room: 1 do
    
    bits_no = [2,4,8,16].sample
    sleep 1
    with_fx :bitcrusher, bits: bits_no, cutoff: 120, mix: 0.125 do
      use_synth :pnoise
      play :c1, amp: noise_vol, sustain: 0.5, release: 0.5
      sleep 2
      
      use_synth :bnoise
      play :c4, amp: noise_vol * 10, sustain: 0.25, release: 0.2
      sleep 0.5
      
      use_synth :noise
      play :c3, amp: noise_vol * 6, sustain: 0.25, release: 0.42
      sleep 0.5
      
      
    end
    sleep 4
  end
end

live_loop :mod_chord do
  sync :base
  sleep 4
  with_fx :reverb, room: 1 do
    with_fx :echo, mix: 0.4 ,phase: 3, decay: 0.8 do
      use_synth :mod_fm
      play :c3, amp: mod_chord_vol, sustain: 0.25, release: 0.1, pan: 1
      sleep 12
    end
  end
end


live_loop :industrial do
  sync :base
  with_fx :lpf do
    with_fx :echo, mix: 0.5 do
      sample :loop_industrial, pitch: -6, env_curve: 3, beat_stretch: 8, amp: industrial_vol,
        release: 0.5, pan: 1
      sleep 8
    end
  end
end

live_loop :bd do
  sync :base
  with_fx :tremolo, mix: 0.7 do
    if (spread 7, 16).tick
      sample :drum_bass_hard, amp: drum_bd_vol, pan: 0.2
    else
      sample :drum_bass_soft, amp: drum_bd_vol * 0.8, pan: 0.2
    end
  end
  sleep 0.5
end

live_loop :sd do
  sync :base
  sample :drum_snare_soft, amp: drum_sd_vol * 0.25, pan: 0.5
  sleep 0.25
  sample :drum_snare_soft, amp: drum_sd_vol * 0.75, pan: 0.5
  sleep 1.75
  with_fx :echo, mix: 0.8 do
    sample :drum_snare_soft, amp: drum_sd_vol * 2, pan: -1
    sleep 4
  end
  sample :drum_snare_soft, amp: drum_sd_vol * 1, pan: -0.5
  sleep 0.25
  sample :drum_snare_soft, amp: drum_sd_vol * 0.5
  sleep 1.75
end


live_loop :hat do
  with_fx :flanger, feedback: 0.5, mix: 0.9 do
    if (spread 1, 8).tick
      
      sample :drum_cymbal_hard, finish: 0.5, amp: drum_hat_vol
      sleep 0.5
    else
      sample :drum_cymbal_closed, finish: 0.25, amp: drum_hat_vol * 1.3
      sleep 0.25
      sample :drum_cymbal_closed, finish: 0.25, amp: drum_hat_vol * 0.6
      sleep 0.25
    end
  end
  
end

