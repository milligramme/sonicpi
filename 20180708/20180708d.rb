live_loop :melody do
  use_synth :pretty_bell
  sync :beats
  
  with_fx :reverb do |r|
    
    12.times do |n|
      play rrand(72,80), sustain: 0.2, decay: 0.15, amp: rrand(0.1,0.4), release: 1
      sleep [0.75, 0.25, 0.5, 1].sample
    end
  end
end

notes = (ring :E4, :Fs4, :B4, :Cs5, :D5, :Fs4, :E4, :Cs5, :B4, :Fs4, :D5, :Cs5)
live_loop :slow do
  play notes.tick, release: 0.1
  sleep 0.3
end
live_loop :faster do
  play notes.tick, release: 0.1
  sleep 0.295
end

live_loop :beats do
  sample :bd_klub
  sleep 0.5
end

live_loop :rev_bd do
  sync :bass_drum
  use_synth :chipnoise
  play 49, amp: 0.1, attack: 0.75, sustain: 6, release: 0.1
  sleep 0.5
end