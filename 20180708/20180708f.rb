use_bpm 90
c = %w{
  major
  major7
  major
  minor
  minor7
  dom7
  major
  dim7
}.map(&:to_sym).ring

live_loop :liff do
  ##| sample :guit_em9, time_dis: 2, clamp_time: 2, compress: 1
  sleep 2.5
  ##|   play chord(:c4, :M), sustain: 0.5, release: 2
  ##|   sleep 2.25
  ##|   play chord(:c4, :m), amp: 0.6, sustain: 0.25, release: 0.1
  ##|   sleep 0.25
  ##|   play chord(:f4, :m), amp: 0.7, sustain: 0.5, release: 0.3
  ##|   sleep 0.25
  ##|   play chord(:d4, :m), amp: 0.6, sustain: 0.25, release: 0.2
  ##|   sleep 0.25
  ##|   play chord(:f4, :m), amp: 0.3, sustain: 0.25, release: 0.5
  ##|   sleep 0.25
end

live_loop :cdes do
  ##| sync :liff
  
  
  use_synth :tri
  with_fx :reverb, mix: 0.8, damp: 0.75, room: 1 do
    with_fx :echo, phase: 0.5, amp: 1, mix: 0.6 do
      play chord(rrand_i(50,56), c.tick), release: 1
      puts c.look
      sleep 2
    end
  end
end