live_loop :cow do
  sync :bd
  # sample :ambi_choir, rate: 1, amp: 3
  sample :drum_cowbell, amp: 0.5
  sleep 1
end

live_loop :bd do
  sample :drum_bass_soft, amp: 3
  sleep 0.5
end


live_loop :random do
  sync :cow
  use_synth :blade
  12.times do
    play rrand(60,84)
    sleep 0.25
  end
end

live_loop :alpe do
  sync :pon
  use_synth :hollow
  12.times do |n|
    play 60 + n
    sleep 0.125
  end
  
end

live_loop :pon do
  sync :bd
  use_synth :piano
  
  play 62
  sleep 0.25
  
  play 62
  sleep 0.75
  
  play 62
  sleep 0.5
  
  play 60
  sleep 0.25
  
  play 72
  sleep 0.75
  
  play 62
  sleep 0.5
  
  play 66
  sleep 0.5
end
