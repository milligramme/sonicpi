use_bpm 90

live_loop :bell do
  use_synth :pluck
  with_fx :reverb, room: 1 do
    play chord([:g4,:a4, :f4].choose, [:minor, :m7].choose), amp: ring(1.5,0.7,0.6,0.7).tick
    sleep 0.5
  end
end

live_loop :crop do
  use_synth :sine
  
  with_fx :reverb, room: 1 do
    with_fx :whammy, grainsize: 4 do
      play :c4, amp: 0.16, attack: 0
    end
  end
  
  sleep 1
end

live_loop :chod do
  use_synth :piano
  ##| with_fx :whammy, grainsize: 2, deltime: 0.4 do
  play chord(:c4, '9sus4'), attack: 0.25, amp: 1.7
  ##| end
  
  
  sleep 2
end
