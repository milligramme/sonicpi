use_bpm 120
live_loop :bass do
  use_synth :sine
  with_fx :bitcrusher, bits: 8, mix: 0.7, cutoff: 120 do
    with_fx :distortion, feedback: 0.8, mix: 0.9 do
      play :c2, attack: 0.0, sustain: 0.01, decay: 0.12, release: 0.25, amp: 2
      sleep 1
    end
  end
end

live_loop :tone do
  use_synth :pretty_bell
  with_fx :reverb, mix: 1 do
    with_fx :krush, cutoff: 120 do
      play :c3, env_curve: 3
      sleep 1
      play :e4, env_curve: 2
      sleep 1
      play :f4, env_curve: 1
      sleep 1
      play :a3, release: 1
      sleep 1
      
    end
  end
end

