use_bpm 120

live_loop :beats do
  sync :hi
  sample :bd_fat, amp: 2
  sleep 1
  
end# Welcome to Sonic Pi v2.10

live_loop :hi do
  with_fx :flanger, delay: 1, feedback: 0.5 do
    sample :drum_cymbal_closed, sustain: 0.1, release: 0.2, amp:[0.6, 0.3, 0.7, 0.2].ring.tick
    sleep 0.25
  end
end

live_loop :cowb do
  sync :beats
  ##| sample :ambi_glass_hum, amp: 0.75, slice: [2.25, 1].ring.tick
  sample :bass_hit_c, pitch: [1.2, 5.4, 3.6].ring.tick
  
  sleep 4
end