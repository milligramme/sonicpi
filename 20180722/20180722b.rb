use_bpm 120

live_loop :timing do
  
  sleep 1
end


live_loop :p1 do
  sync :timing
  use_synth :pretty_bell
  
  with_fx :reverb, room: 1 do
    with_fx :echo, decay: 0.5, mix: 0.5 do
      play_pattern_timed chord(:c2, :minor7), [0.1, 0.5, 0.8, 0.2]
      play_pattern [:c2, :b2, :a3], [0.1, 0.5, 0.8]
      play_pattern_timed chord(:g2, '7'), 0.2
    end
  end
  sleep 1
end


live_loop :b1 do
  sync :timing
  use_synth :sine
  
  with_fx :flanger, feedback: (knit 10, 20, 1, 1).choose do
    play chord(:g3, :M), attack: 0.4, sustain: 0.3, decay: 0.3, release: 1
    sleep 0.5
    play chord(:f5, '7'), attack: 0, sustain: 0.3, decay: 0.3, release: 0.5
    sleep 0.7
  end
  play chord(:a4, :sus2), attack: 0.4, sustain: 0, decay: 0.3, release: 0.6
  sleep 0.3
  play chord(:b2, :minor), attack: 0.24, sustain: 0.3, decay: 0.3, release: 1
  
  sleep 1.5
end

