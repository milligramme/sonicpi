use_bpm 60

live_loop :c1 do
  use_synth :sine
  with_fx :echo, mix: 0.2 do
    play_pattern_timed chord(:c4, :M), 0.25
    play_pattern_timed chord(:g4, '7'), 0.25
    play_pattern_timed chord(:f4, :M), 0.25
    play_pattern_timed chord(:a4, :M), 0.25
  end
end
