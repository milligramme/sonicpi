use_bpm 90




live_loop :clock do
  ##| sample :bass_dnb_f
  sleep 1
end

live_loop :ha do
  sync :clock
  with_fx :echo, mix: 0.8, phase: 0.75, pre_mix: [0.2,0.9].sample do
    dice 8
    case dice 20# rrand_i(0, 10)
    when 0 then sample :tabla_ghe3
    when 1 then sample :tabla_ghe4
    when 2
      use_synth :piano
      play chord [:c8, :f8, :e7, :a8, :b8].sample, :m , amp: 3.5
    when 3 then sample :tabla_ke3
    when 4 then sample :tabla_ghe2
    when 5 then sample :tabla_ghe5
    when 6 then sample :tabla_ke2
    when 7 then sample :tabla_dhec
    else sample :tabla_na_s
      sleep 1
    end
  end
end


live_loop :bell do
  with_synth :pretty_bell do
    with_fx :eq do
      with_fx :tremolo, mix: 0.5 do
        with_fx :reverb, room: 1, mix: rrand(0.25, 0.5) do
          play [:c3, :c3, :a3, :a2, :c3, :c3, :f3, :a2].ring.tick
          sleep 1
        end
      end
    end
  end
  
end
