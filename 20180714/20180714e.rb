use_bpm 120

# donbrako..

live_loop :tick do
  use_synth :pretty_bell
  with_fx :flanger, feedback: rrand(0.2, 0.3), mix: rrand(0.3, 0.9) do
    with_fx :echo, decay: [0.25, 0.5].choose do
      play :c3, amp: (ring 0.1, 0.4, 0.2, 0.3, 0.05, 0.3).tick, attack: 0, sustain: 0.3, release: 0.3
    end
  end
  sleep 0.5
end



live_loop :don do
  sync :tick
  use_synth :beep
  with_fx :reverb, room: 1 do
    play :a1, attack: 0.25, release: 0.3, amp: 2
  end
  sleep 2
end


live_loop :bra do
  sync :don
  use_synth :beep
  sleep 0.75
  
  with_fx :slicer, phase: 0.7, mix: 0.8 do
    play chord(:f2, :minor), decay: 0.2, release: 1.1
    play chord(:g2, :sus4), sustain: 0.6, release: 1.3
  end
  sleep 1
end


live_loop :ko do
  sync :don
  sleep 1.25
  
  with_fx :flanger, decay: 0.4, feedback: 0.25 do
    use_synth :beep
    play :g2, attack: 0.2, decay: 0.2, release: 0.6, amp: 0.5
  end
  sleep 0.75
end


