# Welcome to Sonic Pi v2.10
use_bpm 90

live_loop :base do
  use_synth :pretty_bell
  8.times do
    play chord([:c4, :c7, :c6, :c3].choose, :major ), amp: 0.6, release: 0.5
    sleep 1
  end
end


live_loop :melody do
  sync :base
  with_fx :distortion, mix: 0.5 do
    sample :loop_tabla, amp: 4
  end
  sleep 4
end


