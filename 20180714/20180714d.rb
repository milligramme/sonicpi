use_bpm 90

live_loop :do do
  with_fx :gverb, mix: 0.6, room: 9, pre_amp: 0.6, amp: 1, dry: 0.76 do
    sleep 6
    with_fx :bitcrusher, bits: 4 do
      with_transpose 3 do
        play chord rrand(60,71), :minor
      end
    end
    sleep 2
  end
end